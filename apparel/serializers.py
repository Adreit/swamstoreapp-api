import copy
from rest_framework import serializers
from apparel.models import Clothe


class ClotheSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Clothe
        fields = ('name', 'description', 'availability','price', 'photo', 'size', 'url')
        

class SizeMenSerializer(serializers.Serializer):
    chest = serializers.DecimalField(max_digits=3, decimal_places=1)
    waist = serializers.DecimalField(max_digits=3, decimal_places=1)
    neck = serializers.DecimalField(max_digits=3, decimal_places=1)
    hip = serializers.DecimalField(max_digits=3, decimal_places=1)
    shoulder = serializers.DecimalField(max_digits=3, decimal_places=1)

    def validate(self, data):
        size = None
        if((14<=float(data['neck'])<15) and (35<=float(data['chest'])<38)and(29<float(data['waist'])<32)and (39<=float(data['hip'])<=40)or(16.5<=float(data['shoulder'])<=16.9)):
            size="S"
        elif(15<=float(data['neck'])<16)and (38<=float(data['chest'])<42)and(33<=float(data['waist'])<36)and(41<=float(data['hip'])<42)or(17.3<=float(data['shoulder'])<=17.7):
            size="M"
        elif(16<=float(data['neck'])<17)and(42<=float(data['chest'])<45)and(37<=float(data['waist'])<40)and(44<=float(data['hip'])<47)or(18.1<=float(data['shoulder'])<=18.5):
            size="L"
        elif(17<=float(data['neck'])<18)and(46<=float(data['chest'])<49)and(41<=float(data['waist'])<44)and(48<=float(data['hip'])<51)or(18.9<=float(data['shoulder'])<=19.4):
            size="XL"
        elif(18<=float(data['neck'])<19)and(50<=float(data['chest'])<53)and(45<=float(data['waist'])<48)and(52<=float(data['hip'])<55)or(19.9<=float(data['shoulder'])<=20.4):
            size="XXL"
        elif(19<=float(data['neck'])<=19.5)and(53<=float(data['chest'])<55)and(49<=float(data['waist'])<52)and(56<=float(data['hip'])<=57)or(20.9<=float(data['shoulder'])<=21.4):
            size="XXXL"
        else:
            msg = 'You entered invalid values. Please verify if your measumement are correct and enter them again'
            raise serializers.ValidationError(msg)
        
        validated_data = copy.deepcopy(data)
        validated_data.setdefault('size', size)
        return validated_data

    def to_representation(self, validated_data):
        return {'size': validated_data['size']}
            

class SizeWomenSerializer(serializers.Serializer):
    chest=serializers.DecimalField(max_digits=3, decimal_places=1)
    waist=serializers.DecimalField(max_digits=3, decimal_places=1)
    hip=serializers.DecimalField(max_digits=3, decimal_places=1)
    inseam=serializers.DecimalField(max_digits=3, decimal_places=1)
    thigh=serializers.DecimalField(max_digits=3, decimal_places=1)

    class Meta:
        fields = ('chest', 'waist','hip', 'inseam','thigh')

    def validate(self, data):
        size = None;
        if((31.5<=float(data['chest'])<33.5) and (23<=float(data['waist'])<=24)and(33.5<=float(data['hip'])<36)and (29<float(data['inseam'])<=43)and (20<float(data['thigh'])<20.8)):
            size="XS"
        elif(33.5<=float(data['chest'])<35.5)and (25<=float(data['waist'])<=26)and(36<=float(data['hip'])<38)and(30<float(data['inseam'])<=31)and (20.8<=float(data['thigh'])<21.3):
            size="SM"
        elif(35.5<=float(data['chest'])<38)and(27<= float(data['waist'])<29.5)and(38<= float(data['hip'])<40.5)and(31<float(data['inseam'])<=32)and (21.3<=float(data['thigh'])<22.5):
            size="MD"
        elif(38<=float(data['chest'])<40)and(29.5<= float(data['waist']) < 31)and(40.5<= float(data['hip'])<42)and(32< float(data['inseam'])<33)and (22.6< float(data['thigh'])<24):
            size="LG"
        elif(40<=float(data['chest'])<=43)and(31<= float(data['waist'])<35)and(42<= float(data['hip'])<46)and(33<= float(data['inseam'])<=34)and (24<= float(data['thigh'])<26):
            size="XL"
        elif(44<=float(data['chest'])== 60)and(35<= float(data['waist'])<=37)and(46<= float(data['hip'])<=48)and(34< float(data['inseam'])<=35)and (26<= float(data['thigh'])<29):
            size="XXL"
        else:
            msg = 'You entered invalid values. Please verify if your measumement are correct and enter them again'
            raise serializers.ValidationError(msg)

        validated_data = copy.deepcopy(data)
        validated_data.setdefault('size', size)
        return validated_data

    def to_representation(self, validated_data):
        return {'size': validated_data['size']}
