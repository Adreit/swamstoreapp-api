from django.db import models
from django.utils import timezone

class Clothe(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True)
    availability = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    photo = models.URLField()
    url = models.URLField()
    size = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    gender = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.name
