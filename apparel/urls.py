from rest_framework import routers
from django.conf.urls import url
from apparel.views import sizeMen, sizeWomen, ClotheViewSet


router = routers.SimpleRouter()
router.register(r'clothes', ClotheViewSet, 'clothe')

urlpatterns = router.urls + [
    url(r'^sizeWomen', sizeWomen),
    url(r'^sizeMen', sizeMen)
]
