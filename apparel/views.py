
from rest_framework import viewsets, mixins
from apparel.models import Clothe
from apparel.serializers import SizeMenSerializer, SizeWomenSerializer, ClotheSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response


class ClotheViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = ClotheSerializer

    def get_queryset(self):
        queryset = Clothe.objects.all()

        if 'size' in self.request.query_params:
            queryset = queryset.filter(size=self.request.query_params['size'])

        if 'gender' in self.request.query_params:
            queryset = queryset.filter(gender=self.request.query_params['gender'])
        
        return queryset


@api_view(['POST'])
def sizeMen(request):
    serializer = SizeMenSerializer(data=request.data)
    if serializer.is_valid():
        return Response(serializer.data)
    return Response(serializer.errors, status=400)   
    return Response(size)



@api_view(['POST'])
def sizeWomen(request):
    serializer = SizeWomenSerializer(data=request.data)
    if serializer.is_valid():
        return Response(serializer.data)
    return Response(serializer.errors, status=400)   